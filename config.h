
#define QUEUEKEYLEN         160
#define QUEUELEN            100
#define MAXPAGES            10000
#define MESSAGELEN          1024
#define USERHASHSIZE        100
#define UNIX_SOCKET         "/var/run/queued/queue_fcgi"
/*#define SOCKET              "localhost:9000"*/
#define BACKLOG             10000
#define AUTH_KEY_NAME       "JSESSIONID"
#define AUTH_KEY_LEN        32
#define AUTH_MAP_SIZE       10000
#define REQUESTIDLEN        32
#define JSCALLBACK          "konferoAjaxCallback"
#define EAWAYINT            300000
#define EDISCONNECTEDINT    30000
#define EFOLLOWREQUEST      180000


#define DBHOST          "localhost"
#define DBUSER          "root"
#define DBPASSWD        NULL
#define DBDB            "konfero"
#define DBPORT          0
#define DBUNIXSOCKET    "/var/run/mysqld/mysqld.sock"
#define USER_TABLE      "queue_user"


#define AH_DBHOST       "localhost"
#define AH_DBUSER       "root"
#define AH_DBPASSWD     NULL
#define AH_DBDB         "konfero_history"
#define AH_DBPORT       0
#define AH_DBUNIXSOCKET "/var/run/mysqld/mysqld.sock"
#define AH_TABLE        "message_history"
#define UH_TABLE        "user_history"
#define CH_TABLE        "conversation_history"
#define PCH_TABLE       "public_conversation_history"
#define AH_CONV_INT     21600000
/*#define AH_INDEXURL     "http://192.168.14.17:8888/history/"*/

