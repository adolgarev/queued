#ifndef AHISTORY_H
#define AHISTORY_H


#include <mpool.h>
#include <mysql/mysql.h>


#include <curl/curl.h>


#include "queue.h"



struct addmessage_s {
    u_int32_t   id;
    u_int64_t   time;
    char        message_type[MESSAGETYPELEN];
    char        queue_id[QUEUEKEYLEN];
    char        buf[MESSAGELEN];
};

typedef struct addmessage_s addmessage_t;




struct ahistory_bind_s {
    enum enum_field_types   buffer_type;
    unsigned long           len;
    char                   *name;
    long                    long_val;
};

typedef struct ahistory_bind_s ahistory_bind_t;









typedef enum {
    AH_ERROR = 1,
    AH_NOT_FOUND
} ahistory_error_e;

typedef u_int8_t ahistory_error_t;




struct ahistory_list_s {
    struct ahistory_list_s *next;
    struct ahistory_list_s *prev;

    message_t   data;
};

typedef struct ahistory_list_s ahistory_list_t;





struct ahistory_in_s {
    /*mpool_t    *pool;*/

    ahistory_list_t    *head;
    ahistory_list_t    *end;

    pthread_mutex_t     list_mutex;

    MYSQL      *mysql;
    MYSQL_STMT *insert_stmt;
    MYSQL_STMT *select_conversation_stmt;
    MYSQL_STMT *update_conversation_stmt;

    MYSQL_STMT *insert_public_conversation_stmt;
    MYSQL_STMT *select_public_conversation_stmt;
    MYSQL_STMT *update_public_conversation_stmt;

    CURL *curl;

};

typedef struct ahistory_in_s ahistory_in_t;



int
ahistory_init(ahistory_in_t *a);

int
ahistory_close(ahistory_in_t *a);

int
ahistory_append(ahistory_in_t *a, message_t *m);

int
ahistory_consume(ahistory_in_t *a, message_t *m);


void*
ahistory(void *arg);

#endif
