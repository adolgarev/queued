
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>

#include <pthread.h>

#include "ahistory.h"
#include "queue.h"





#define USER_INSERT_OR_UPDATE_STMT "INSERT INTO " USER_TABLE "(queueId, userId, status, time, followUserId, followIndex) VALUES(?,?,?,?,?,?) ON DUPLICATE KEY UPDATE status=VALUES(status), time=VALUES(time), followUserId=VALUES(followUserId), followIndex=VALUES(followIndex)"



























void*
event_thread(void *arg) {

    queue_mgm_t    *q;



    q = arg;



    mysql_thread_init();



    while(1) {

        event_type_t    event_type;


        for (event_type = 0; event_type < EVENTTYPENUM; event_type++) {

            ilist_t    *il;

            event_key_t    *event_key;
            event_t        *e;

            struct timeval  tv;
            long            time;



            il = q->event_list[event_type];








            while (1) {
                
                char    queue_id[QUEUEKEYLEN];
                size_t  queue_id_len;


                pthread_mutex_lock(&q->event_list_mutex);

                gettimeofday(&tv, NULL);
                time = tv.tv_sec * 1000 + tv.tv_usec / 1000;


                if (!il->head) {
                    pthread_mutex_unlock(&q->event_list_mutex);
                    break;
                }

                event_key   = (event_key_t*)il->head->key;
                e           = (event_t*)(il->head->key + sizeof(event_key_t));

                if (time < e->time) {
                    pthread_mutex_unlock(&q->event_list_mutex);
                    break;
                }


                strcpy(queue_id, event_key->queue_id);
                queue_id_len = strlen(queue_id);

                
                /* send message to the queue */
                if (event_type == E_AWAY || event_type == E_DISCONNECTED) {
                    message_t   m;

                    memset(&m, 0, sizeof(message_t));
                    m.status            = S_UNKNOWN;

                    strcpy(m.queue_id, event_key->queue_id);
                    m.source_user_id    = event_key->source_user_id;
                    m.time              = time;
                    if (event_type == E_AWAY) {
                        strcpy(m.message_type, M_USER_STATUS_CHANGED);
                        m.status        = S_AUTO_AWAY;
                    }
                    else if (event_type == E_DISCONNECTED) {
                        strcpy(m.message_type, M_USER_LEFT);


                        /* DEBUG */
                        /*printf("event E_DISCONNECTED source_user_id=%ld queue_id=%s time=%ld\n", event_key->source_user_id, event_key->queue_id, e->time);*/
                    }
















                    ilist_del(il, (char*)event_key, sizeof(event_key_t), 0);







                    pthread_mutex_unlock(&q->event_list_mutex);


                    if (queue_put(q, queue_id, queue_id_len, NULL, &m)) {
                        fprintf(stderr, "Server Error: cannot send message\n");
                        goto error;
                    }
                }
                else if (event_type == E_FOLLOWREQUEST) {
                    follow_event_t *fe;
                    message_t       m;
                    message_t       m1;


                    fe = (follow_event_t*)e;


                    memset(&m, 0, sizeof(message_t));
                    m.status            = S_UNKNOWN;

                    strcpy(m.queue_id, event_key->queue_id);
                    m.source_user_id    = event_key->source_user_id;
                    m.time              = time;
                    strcpy(m.message_type, M_FOLLOW_REQUEST_EXPIRED);
                    strcpy(m.buf, "destUserId");
                    sprintf(m.buf + 11, "%ld", fe->dest_user_id);


                    memcpy(&m1, &m, sizeof(message_t));
                    strcpy(m1.queue_id, fe->response_queue_id);


                    ilist_del(il, (char*)event_key, sizeof(event_key_t), 0);

                    pthread_mutex_unlock(&q->event_list_mutex);


                    if (queue_put(q, queue_id, strlen(queue_id), NULL, &m)) {
                        fprintf(stderr, "Server Error: cannot send message\n");
                        goto error;
                    }

                    if (queue_put(q, m1.queue_id, strlen(m1.queue_id), NULL, &m1)) {
                        fprintf(stderr, "Server Error: cannot send message\n");
                        goto error;
                    }

                }





            }





        }

error:


        sleep(1);
    }




    mysql_thread_end();



    return NULL;
}

























static int
user_get(queue_t *q, auth_val_t id, user_t **up) {

    int n;
    user_t *u;


    n = id % USERHASHSIZE;

    u = q->user_hash[n];
    while (u) {
        if (u->id == id) {
            *up = u;
            return 0;
        }

        u = u->next;
    }


    *up = NULL;


    return 1;
}


static int
user_del(queue_t *q, auth_val_t id) {

    int n;
    user_t *p;
    user_t *u;


    n = id % USERHASHSIZE;

    p = NULL;
    u = q->user_hash[n];
    while (u) {
        if (u->id == id) {
            if (p)
                p->next = u->next;
            else
                q->user_hash[n] = NULL;

            free(u);

            return 0;
        }

        p = u;
        u = u->next;
    }



    return 1;
}


static int
user_put(queue_t *q, user_t *up) {
    
    int n;
    user_t *p;
    user_t *u;


    n = up->id % USERHASHSIZE;

    p = NULL;
    u = q->user_hash[n];
    while (u) {
        if (u->id == up->id) {
            up->next = u->next;
            memcpy(u, up, sizeof(user_t));

            return 0;
        }

        p = u;
        u = u->next;
    }


    u = malloc(sizeof(user_t));
    if (!u) {
        perror("malloc");
        return 1;
    }

    memcpy(u, up, sizeof(user_t));
    u->next = NULL;

    if (p)
        p->next = u;
    else
        q->user_hash[n] = u;


    
    return 0;
}
















static int
user_insert_or_update(queue_mgm_t *qp, char *key, size_t key_len, user_t *u, long time) {

    MYSQL          *mysql;
    MYSQL_STMT     *stmt;
    MYSQL_BIND      bind[6];
    my_bool         null4;
    my_bool         null5;




    unsigned long mysql_tid;



    mysql = qp->mysql;


#if 1
    mysql_tid = mysql_thread_id(mysql);
    if (mysql_ping(mysql)) {
        printf("MySQL ping error: Error: %s\n", mysql_error(mysql));
        goto error;
    }
    if (mysql_tid != mysql_thread_id(mysql)) {

        /* a reconnect occured, stmt was released */
        if (!(qp->insert_or_update_stmt = mysql_stmt_init(mysql))) {
            fprintf(stderr, "mysql_stmt_init(), out of memory\n");
            goto error;
        }
        if (mysql_stmt_prepare(qp->insert_or_update_stmt, USER_INSERT_OR_UPDATE_STMT, strlen(USER_INSERT_OR_UPDATE_STMT))) {
            fprintf(stderr, "%s\n", mysql_stmt_error(qp->insert_or_update_stmt));
            goto error;
        }
    }
#endif

    stmt = qp->insert_or_update_stmt;

    
    memset(bind, 0, sizeof(bind));

    null4 = 1;
    null5 = 1;


    bind[0].buffer_type     = MYSQL_TYPE_STRING;
    bind[0].buffer          = key;
    bind[0].buffer_length   = key_len;
    bind[0].is_null         = 0;
    bind[0].length          = &key_len;

    bind[1].buffer_type     = MYSQL_TYPE_LONG;
    bind[1].buffer          = (char*)&u->id;
    bind[1].is_null         = 0;
    bind[1].length          = 0;

    bind[2].buffer_type     = MYSQL_TYPE_LONG;
    bind[2].buffer          = (char*)&u->status;
    bind[2].is_null         = 0;
    bind[2].length          = 0;

    bind[3].buffer_type     = MYSQL_TYPE_LONGLONG;
    bind[3].buffer          = (char*)&time;
    bind[3].is_null         = 0;
    bind[3].length          = 0;

    bind[4].buffer_type     = MYSQL_TYPE_LONG;
    bind[4].buffer          = u->follow_user_id ? (char*)&u->follow_user_id : NULL;
    bind[4].is_null         = u->follow_user_id ? 0 : &null4;
    bind[4].length          = 0;

    bind[5].buffer_type     = MYSQL_TYPE_LONG;
    bind[5].buffer          = u->follow_index ? (char*)&u->follow_index : NULL;
    bind[5].is_null         = u->follow_index ? 0 : &null5;
    bind[5].length          = 0;


    if (mysql_stmt_bind_param(stmt, bind)) {
        fprintf(stderr, "%s\n", mysql_stmt_error(stmt));
        goto error;
    }

    if (mysql_stmt_execute(stmt)) {
        fprintf(stderr, "%s\n", mysql_stmt_error(stmt));
        goto error;
    }







    return 0;


error:

    return 1;
}







void
queue_alloc_cb(char *data, size_t data_len) {
    queue_t        *q;


    
    q = (queue_t*)data;

    memset(q, 0, sizeof(queue_t));


}


void
queue_free_cb(char *data) {
    queue_t        *q;

    int i;

    
    q = (queue_t*)data;



    for (i = 0; i < USERHASHSIZE; i++) {
        user_t *u;

        u = q->user_hash[i];
        while (u) {
            user_t *n;

            n = u->next;
            free(u);
            u = n;
        }
    }

    




}


int
queue_mgm_open(queue_mgm_t **qp, ahistory_in_t *ain, queue_flags_t flags) {
    
    queue_mgm_t    *q;
    ilist_t        *il;

    MYSQL      *mysql;
    MYSQL_STMT *insert_or_update_stmt;



    q = malloc(sizeof(queue_mgm_t));
    if (!q)
        goto error;
    *qp = q;

    memset(q, 0, sizeof(queue_mgm_t));
    

    if (ilist_open(&il, QUEUEKEYLEN, sizeof(queue_t), getpagesize(), MAXPAGES, queue_alloc_cb, queue_free_cb, IL_NOPOOL))
        goto error;
    q->il = il;


    q->ain = ain;





    if(!(mysql = mysql_init(NULL))) {
        printf("Failed to initate MySQL connection\n");
        goto error;
    }
    q->mysql = mysql;

    
    my_bool reconnect = 1;
    mysql_options(mysql, MYSQL_OPT_RECONNECT, &reconnect);

    
    if (!mysql_real_connect(mysql, DBHOST, DBUSER, DBPASSWD, DBDB, DBPORT, DBUNIXSOCKET, 0)) {
        printf("Failed to connect to MySQL: Error: %s\n", mysql_error(mysql));
        goto error;
    }


    /*mysql_autocommit(mysql, 0);*/

    if (mysql_query(mysql, "SET SESSION TRANSACTION ISOLATION LEVEL READ COMMITTED")) {
        printf("Failed to set transaction isolation level: Error: %s\n", mysql_error(mysql));
        goto error;
    }


    if (!(insert_or_update_stmt = mysql_stmt_init(mysql))) {
        fprintf(stderr, "mysql_stmt_init(), out of memory\n");
        goto error;
    }
    q->insert_or_update_stmt = insert_or_update_stmt;
    if (mysql_stmt_prepare(insert_or_update_stmt, USER_INSERT_OR_UPDATE_STMT, strlen(USER_INSERT_OR_UPDATE_STMT))) {
        fprintf(stderr, "%s\n", mysql_stmt_error(insert_or_update_stmt));
        goto error;
    }


    mysql_thread_init();



    /*pthread_mutexattr_t     attr;
    pthread_mutexattr_init(&attr);
    if (pthread_mutexattr_settype(&attr, PTHREAD_MUTEX_RECURSIVE)) {
        perror("pthread_mutexattr_settype");
        goto error;
    }*/
    if (pthread_mutex_init(&q->event_list_mutex, NULL)) {
        fprintf(stderr, "Error in pthread_mutex_init\n");
        goto error;
    }
    /*pthread_mutexattr_destroy(&attr);*/


    if (ilist_open(&q->event_list[E_AWAY], sizeof(event_key_t), sizeof(event_t), 0, 0, NULL, NULL, IL_NOPOOL))
        goto error;
    if (ilist_open(&q->event_list[E_DISCONNECTED], sizeof(event_key_t), sizeof(event_t), 0, 0, NULL, NULL, IL_NOPOOL))
        goto error;
    if (ilist_open(&q->event_list[E_FOLLOWREQUEST], sizeof(event_key_t), sizeof(follow_event_t), 0, 0, NULL, NULL, IL_NOPOOL))
        goto error;



    pthread_t   event_tid;
    pthread_create(&event_tid, NULL, event_thread, q);


    
    return 0;


error:
    
    return Q_ERROR;
}




int
queue_mgm_close(queue_mgm_t *qp) {

    int i;


    ilist_close(qp->il);


    mysql_thread_end();

    mysql_stmt_close(qp->insert_or_update_stmt);

    mysql_close(qp->mysql);

    
    
    pthread_mutex_destroy(&qp->event_list_mutex);


    for (i = 0; i < EVENTTYPENUM; i++) {
        ilist_close(qp->event_list[i]);
    }
    
    
    
    free(qp);


    return 0;
}


int
queue_get(queue_mgm_t *qp, char *key, size_t key_len, auth_val_t a, int *n, message_t **begin, message_t **end, queue_flags_t flags) {

    ilist_t        *il;
    queue_t        *q;

    int ret;



    il = qp->il;


    pthread_mutex_lock(&qp->event_list_mutex);

    /* move disconnect event to the end */
    long            time;
    {
        event_key_t     event_key;
        event_t        *e;

        struct timeval  tv;

        
        gettimeofday(&tv, NULL);
        time = tv.tv_sec * 1000 + tv.tv_usec / 1000;


        memset(&event_key, 0, sizeof(event_key_t));
        strncpy(event_key.queue_id, key, key_len);
        event_key.source_user_id = a;

        /*pthread_mutex_lock(&qp->event_list_mutex);*/

        ret = ilist_get(qp->event_list[E_DISCONNECTED], (char*)&event_key, sizeof(event_key_t), (char**)&e, IL_MOVE_TO_END);
        if (ret == IL_NOT_FOUND)
            ;
        else if (!ret)
            e->time = time + EDISCONNECTEDINT;

        /*pthread_mutex_unlock(&qp->event_list_mutex);*/
    }








    /* IF_CREATE if enter chatroom on getMessages */
    /*ret = ilist_get(il, key, key_len, (char**)&q, IL_CREATE);*/
    ret = ilist_get(il, key, key_len, (char**)&q, 0);
    if (ret == IL_NOT_FOUND) {
        *n      = 0;
        q       = NULL;
        *begin  = NULL;
        *end    = NULL;
    }
    else if (ret)
        goto error;
    else {
        if (*n == -1)
            *n = q->n;
        *begin  = q->messages + *n;
        if (q->n >= *n) {
            *end    = q->messages + q->n;
            *n      = q->n;
        }
        else if (*n > q->n) {
            *end    = q->messages + QUEUELEN;
            *n      = 0;
        }
    }









    /* enter chatroom */
    /*if (q) {
        user_t *up;


        user_get(q, a, &up);

        if (!up || up->status == S_DISCONNECTED) {
            message_t   m;

            memset(&m, 0, sizeof(message_t));
            m.status            = S_UNKNOWN;


            strcpy(m.queue_id, key);
            m.source_user_id    = a;
            m.time              = time;
            strcpy(m.message_type, M_USER_ENTERED);
            m.status            = S_AVAILABLE;




            pthread_mutex_unlock(&qp->event_list_mutex);

            if (queue_put(qp, key, key_len, NULL, &m)) {
                fprintf(stderr, "Server Error: cannot send message\n");
                goto error;
            }

            pthread_mutex_lock(&qp->event_list_mutex);


        }
    }*/







    if ((flags & Q_STARTTYPING) || (flags & Q_STOPTYPING)) {
        /* send start/stop typing message */
        message_t   m;

        memset(&m, 0, sizeof(message_t));
        m.status            = S_UNKNOWN;

        strcpy(m.queue_id, key);
        m.source_user_id    = a;
        m.time              = time;
        if (flags & Q_STARTTYPING)
            strcpy(m.message_type, M_START_TYPING);
        else if (flags & Q_STOPTYPING)
            strcpy(m.message_type, M_STOP_TYPING);



        pthread_mutex_unlock(&qp->event_list_mutex);

        if (queue_put(qp, key, key_len, NULL, &m)) {
            fprintf(stderr, "Server Error: cannot send message\n");
            goto error;
        }

        pthread_mutex_lock(&qp->event_list_mutex);
    }












    if (q && (flags & Q_ACTIVE)) {
        user_t *up;


        user_get(q, a, &up);

        if (!up || up->status == S_AUTO_AWAY || up->status == S_AWAY) {
            /* send UserStatusChanged with status = S_AVAILABLE */
            message_t   m;

            memset(&m, 0, sizeof(message_t));
            m.status            = S_UNKNOWN;


            strcpy(m.queue_id, key);
            m.source_user_id    = a;
            m.time              = time;
            strcpy(m.message_type, M_USER_STATUS_CHANGED);
            m.status            = S_AVAILABLE;




            pthread_mutex_unlock(&qp->event_list_mutex);

            if (queue_put(qp, key, key_len, NULL, &m)) {
                fprintf(stderr, "Server Error: cannot send message\n");
                goto error;
            }

            pthread_mutex_lock(&qp->event_list_mutex);


        }

    }









    pthread_mutex_unlock(&qp->event_list_mutex);

    return 0;

error:

    pthread_mutex_unlock(&qp->event_list_mutex);

    return Q_ERROR;
}





int
queue_put(queue_mgm_t *qp, char *key, size_t key_len, int *n, message_t *m) {

    ilist_t        *il;
    queue_t        *q;


    int ret;



    il = qp->il;

    pthread_mutex_lock(&qp->event_list_mutex);


    ret = ilist_get(il, key, key_len, (char**)&q, IL_CREATE | IL_MOVE_TO_END);
    if (ret) {
        fprintf(stderr, "queue_put: ilist_get failed for queue\n");
        goto error;
    }





    {


        if (!*m->message_type) {
            fprintf(stderr, "queue_put: no messageType in message\n");
            goto error;
        }

        if (!m->source_user_id) {
            fprintf(stderr, "queue_put: no sourceUserId in message of type %s\n", m->message_type);
            goto error;
        }

        if (!m->time) {
            fprintf(stderr, "queue_put: no time in message of type %s\n", m->message_type);
            goto error;
        }


        if (!strcmp(m->message_type, M_USER_ENTERED)) {
            user_t  u;
            char   *p;

            memset(&u, 0, sizeof(user_t));
            u.id        = m->source_user_id;
            u.status    = S_AVAILABLE;

            p = message_get_value(m, "followUserId");
            if (p)
                u.follow_user_id = strtoll(p, NULL, 10);

            if (u.follow_user_id) {
                p = message_get_value(m, "followIndex");
                if (p)
                    u.follow_index = strtol(p, NULL, 10);
            }


            if (user_put(q, &u)) {
                fprintf(stderr, "queue_put: user_put failed for user %ld\n", u.id);
                goto error;
            }


            user_insert_or_update(qp, key, key_len, &u, m->time);

            /* create events */
            {
                event_key_t     event_key;
                event_t        *e;

                memset(&event_key, 0, sizeof(event_key_t));
                strncpy(event_key.queue_id, key, QUEUEKEYLEN);
                event_key.source_user_id = m->source_user_id;
                
                /*pthread_mutex_lock(&qp->event_list_mutex);*/

                ret = ilist_get(qp->event_list[E_AWAY], (char*)&event_key, sizeof(event_key_t), (char**)&e, IL_CREATE | IL_MOVE_TO_END);
                e->time = m->time + EAWAYINT;

                ret = ilist_get(qp->event_list[E_DISCONNECTED], (char*)&event_key, sizeof(event_key_t), (char**)&e, IL_CREATE | IL_MOVE_TO_END);
                e->time = m->time + EDISCONNECTEDINT;

                /* DEBUG */
                /*printf("create event E_DISCONNECTED source_user_id=%ld queue_id=%s time=%ld\n", event_key.source_user_id, event_key.queue_id, e->time);*/

                /*pthread_mutex_unlock(&qp->event_list_mutex);*/
            }
        }
        else if (!strcmp(m->message_type, M_USER_LEFT)) {
            user_t  u;
            event_key_t     event_key;
            int     ret;

            user_del(q, m->source_user_id);

            memset(&u, 0, sizeof(user_t));
            u.id        = m->source_user_id;
            u.status    = S_DISCONNECTED;


            user_insert_or_update(qp, key, key_len, &u, m->time);

            /* delete away event */
            memset(&event_key, 0, sizeof(event_key_t));
            strncpy(event_key.queue_id, key, QUEUEKEYLEN);
            event_key.source_user_id = m->source_user_id;

            ret = ilist_del(qp->event_list[E_AWAY], (char*)&event_key, sizeof(event_key_t), 0);
            ret = ilist_del(qp->event_list[E_DISCONNECTED], (char*)&event_key, sizeof(event_key_t), 0);







            {

                int n;
                user_t *u;
                int     ret;




                for (n = 0; n < USERHASHSIZE; n++) {

                    u = q->user_hash[n];
                    while (u) {

                        if (u->follow_user_id == m->source_user_id) {

                            event_key_t     event_key;

                            u->status = S_DISCONNECTED;
                            u->follow_user_id = 0;
                            u->follow_index = 0;


                            user_insert_or_update(qp, key, key_len, u, m->time);


                            /* delete events */
                            memset(&event_key, 0, sizeof(event_key_t));
                            strncpy(event_key.queue_id, key, key_len);
                            event_key.source_user_id = u->id;

                            ret = ilist_del(qp->event_list[E_AWAY], (char*)&event_key, sizeof(event_key_t), 0);
                            ret = ilist_del(qp->event_list[E_DISCONNECTED], (char*)&event_key, sizeof(event_key_t), 0);

                            /* user_del */
                            if (u->next) {
                                user_t *un;

                                un = u->next;
                                user_del(q, u->id);
                                u = un;
                                continue;
                            }
                            else {
                                user_del(q, u->id);
                                break;
                            }

                        }

                        u = u->next;
                    }


                }


            }





        }
        else if (!strcmp(m->message_type, M_USER_STATUS_CHANGED)) {
            user_t  u;
            user_t *up;


            user_get(q, m->source_user_id, &up);


            memset(&u, 0, sizeof(user_t));
            u.id        = m->source_user_id;

            if (m->status == S_UNKNOWN) {
                fprintf(stderr, "queue_put: no status in message of type %s\n", m->message_type);
                goto error;
            }

            u.status    = m->status;
            if (up) {
                u.follow_user_id = up->follow_user_id;
                u.follow_index = up->follow_index;
            }


            if (u.status == S_AUTO_AWAY && up && up->status != S_AVAILABLE) {
                fprintf(stderr, "queue_put: tried to change status to S_AUTO_AWAY of user with status %d\n", up->status);
                goto error;
            }


            if (user_put(q, &u)) {
                fprintf(stderr, "queue_put: user_put failed for user %ld\n", u.id);
                goto error;
            }


            user_insert_or_update(qp, key, key_len, &u, m->time);


            if (u.status == S_AVAILABLE) {
                /* move away event to the end */
                event_key_t     event_key;
                event_t        *e;


                memset(&event_key, 0, sizeof(event_key_t));
                strncpy(event_key.queue_id, key, QUEUEKEYLEN);
                event_key.source_user_id = m->source_user_id;

                /*pthread_mutex_lock(&qp->event_list_mutex);*/

                ret = ilist_get(qp->event_list[E_AWAY], (char*)&event_key, sizeof(event_key_t), (char**)&e, IL_CREATE | IL_MOVE_TO_END);
                e->time = m->time + EAWAYINT;

                /*pthread_mutex_unlock(&qp->event_list_mutex);*/
            }
            else {
                event_key_t     event_key;
                int     ret;

                /* delete away event */
                memset(&event_key, 0, sizeof(event_key_t));
                strncpy(event_key.queue_id, key, QUEUEKEYLEN);
                event_key.source_user_id = m->source_user_id;

                ret = ilist_del(qp->event_list[E_AWAY], (char*)&event_key, sizeof(event_key_t), 0);
            }



        }
        else if (!strcmp(m->message_type, M_FOLLOW_REQUEST)) {
            event_key_t     event_key;
            follow_event_t *e;

            char           *p;
            char           *p1;


            /* create event */

            memset(&event_key, 0, sizeof(event_key_t));
            strncpy(event_key.queue_id, key, QUEUEKEYLEN);
            event_key.source_user_id = m->source_user_id;

            
            p = message_get_value(m, "destUserId");
            if (!p)
                goto error;
            p1 = message_get_value(m, "responseQueueId");
            if (!p1)
                goto error;



            /*pthread_mutex_lock(&qp->event_list_mutex);*/

            ret = ilist_get(qp->event_list[E_FOLLOWREQUEST], (char*)&event_key, sizeof(event_key_t), (char**)&e, IL_CREATE | IL_MOVE_TO_END);
            e->time = m->time + EFOLLOWREQUEST;
            strncpy(e->response_queue_id, p1, QUEUEKEYLEN);
            e->dest_user_id = strtoll(p, NULL, 10);


            /*pthread_mutex_unlock(&qp->event_list_mutex);*/
        }
        else if (!strcmp(m->message_type, M_FOLLOW_REQUEST_CANCELLED)) {
            event_key_t     event_key;


            /* delete event */

            memset(&event_key, 0, sizeof(event_key_t));
            strncpy(event_key.queue_id, key, QUEUEKEYLEN);
            event_key.source_user_id = m->source_user_id;

            /*pthread_mutex_lock(&qp->event_list_mutex);*/

            ilist_del(qp->event_list[E_FOLLOWREQUEST], (char*)&event_key, sizeof(event_key_t), 0);

            /*pthread_mutex_unlock(&qp->event_list_mutex);*/
        }
        else if (!strcmp(m->message_type, M_FOLLOW_RESPONSE)) {
            event_key_t     event_key;

            char           *p;


            p = message_get_value(m, "destUserId");
            if (!p)
                goto error;


            /* delete event */

            memset(&event_key, 0, sizeof(event_key_t));
            strncpy(event_key.queue_id, key, QUEUEKEYLEN);
            event_key.source_user_id = strtoll(p, NULL, 10);

            /*pthread_mutex_lock(&qp->event_list_mutex);*/

            ilist_del(qp->event_list[E_FOLLOWREQUEST], (char*)&event_key, sizeof(event_key_t), 0);

            /*pthread_mutex_unlock(&qp->event_list_mutex);*/
        }
        else  {

            user_t  *up;


            user_get(q, m->source_user_id, &up);
            if (up && (up->status == S_AUTO_AWAY || up->status == S_AWAY)) {
                /* send UserStatusChanged with status = S_AVAILABLE */
                message_t   pm;
                {

                    memset(&pm, 0, sizeof(message_t));
                    pm.status            = S_UNKNOWN;


                    strcpy(pm.queue_id, key);
                    pm.source_user_id    = m->source_user_id;
                    pm.time              = m->time;
                    strcpy(pm.message_type, M_USER_STATUS_CHANGED);
                    pm.status            = S_AVAILABLE;

                }

                pthread_mutex_unlock(&qp->event_list_mutex);

                if (queue_put(qp, key, key_len, NULL, &pm)) {
                    fprintf(stderr, "Server Error: cannot send message\n");
                    goto error;
                }

                pthread_mutex_lock(&qp->event_list_mutex);

            }




            /* move disconnect & away event to the end */
            event_key_t     event_key;
            event_t        *e;


            memset(&event_key, 0, sizeof(event_key_t));
            strncpy(event_key.queue_id, key, QUEUEKEYLEN);
            event_key.source_user_id = m->source_user_id;

            /*pthread_mutex_lock(&qp->event_list_mutex);*/

            ret = ilist_get(qp->event_list[E_AWAY], (char*)&event_key, sizeof(event_key_t), (char**)&e, IL_MOVE_TO_END);
            if (ret == IL_NOT_FOUND)
                ;
            else if (!ret)
                e->time = m->time + EAWAYINT;

            ret = ilist_get(qp->event_list[E_DISCONNECTED], (char*)&event_key, sizeof(event_key_t), (char**)&e, IL_MOVE_TO_END);
            if (ret == IL_NOT_FOUND)
                ;
            else if (!ret)
                e->time = m->time + EDISCONNECTEDINT;
            /*pthread_mutex_unlock(&qp->event_list_mutex);*/

        }
    }




    memcpy(q->messages + q->n, m, sizeof(message_t));
    if (n)
        *n = q->n;
    q->n++;
    if (q->n == QUEUELEN)
        q->n = 0;




    pthread_mutex_unlock(&qp->event_list_mutex);


    /* add to async history */
    if (qp->ain)
        ahistory_append(qp->ain, m);

    return 0;

error:
    
    pthread_mutex_unlock(&qp->event_list_mutex);

    return Q_ERROR;
}



char *
message_get_value(message_t *m, char *name) {

    char   *pos;
    size_t  name_len;



    name_len = strlen(name);

    pos = m->buf;

    while (pos < m->buf + MESSAGELEN && *pos) {
        if (!strcmp(pos, name))
            return pos + name_len + 1;
        else {
            pos += strlen(pos) + 1;
            pos += strlen(pos) + 1;
        }

    }

    return NULL;

}
