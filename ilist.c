


#include <stdlib.h>
#include <string.h>



#include "ilist.h"






int
ilist_open(ilist_t **ilist_p, size_t key_len, size_t data_len, int page_size, size_t max_pages, ilist_alloc_cb alloc_cb, ilist_free_cb free_cb, u_int32_t flags) {

    int ret;
    ilist_t    *il;
    DB         *dbp;
    mpool_t    *pool;


    il = malloc(sizeof(ilist_t));
    if (!il)
        goto error;
    *ilist_p = il;


    memset(il, 0, sizeof(ilist_t));
    il->key_len     = key_len;
    il->data_len    = data_len;
    il->max_pages   = max_pages;
    il->page_size   = page_size;
    il->alloc_cb    = alloc_cb;
    il->free_cb     = free_cb;





    ret = db_create(&dbp, NULL, 0);
    if (ret)
        goto error;
    il->dbp = dbp;

    dbp->set_errfile(dbp, stderr);
    dbp->set_errpfx(dbp, "ilist");

    ret = dbp->open(dbp,
                    NULL,
                    NULL,
                    NULL,
                    DB_BTREE,
                    DB_CREATE,
                    0);
    if (ret) {
        dbp->err(dbp, ret, "%s: db open failed", "ilist");
        goto error;
    }





    if (flags & IL_NOPOOL)
        pool = NULL;
    else {
        pool = mpool_open(0, page_size, NULL, &ret);
        if (pool == NULL) {
            fprintf(stderr, "Error in mpool_open: %s\n", mpool_strerror(ret));
            goto error;
        }
        il->pool = pool;

        if (max_pages) {
            ret = mpool_set_max_pages(pool, max_pages);
            if (ret != MPOOL_ERROR_NONE) {
                fprintf(stderr, "Error in mpool_set_max_pages: %s\n", mpool_strerror(ret));
                goto error;
            }
        }
    }




    

    return 0;


error:

    return IL_ERROR;
}






int
ilist_get(ilist_t *ilist_p, char *key, size_t key_len, char **data, u_int32_t flags) {

    DB     *dbp;
    DBT     dbkey;
    DBT     dbdata;

    int ret;

    char   *val;




    dbp = ilist_p->dbp;



    memset(&dbkey, 0, sizeof(DBT));
    memset(&dbdata, 0, sizeof(DBT));

    dbkey.ulen      = key_len;
    dbkey.flags     = DB_DBT_USERMEM;
    dbkey.data      = key;
    dbkey.size      = key_len;

    dbdata.ulen     = sizeof(val);
    dbdata.flags    = DB_DBT_USERMEM;
    dbdata.data     = &val;


    ret = dbp->get(dbp, NULL, &dbkey, &dbdata, 0);
    if (ret == DB_NOTFOUND) {
        if (flags & IL_CREATE) {
            ilist_data_t   *idata;

            if (ilist_p->pool) {
                idata = mpool_alloc(ilist_p->pool, sizeof(ilist_data_t) + ilist_p->key_len + ilist_p->data_len, &ret);
                if (!idata && ret == MPOOL_ERROR_NO_PAGES) {
                    if (!ilist_p->head)
                        goto error;

                    idata           = ilist_p->head;
                    ilist_p->head   = ilist_p->head->next;


                    if (ilist_p->free_cb)
                        ilist_p->free_cb((char*)idata + sizeof(ilist_data_t) + ilist_p->key_len);


                    memset(&dbkey, 0, sizeof(DBT));

                    dbkey.ulen      = idata->key_len;
                    dbkey.flags     = DB_DBT_USERMEM;
                    dbkey.data      = idata->key;
                    dbkey.size      = idata->key_len;

                    ret = dbp->del(dbp, NULL, &dbkey, 0);
                    if (ret) {
                        dbp->err(dbp, ret, "db del failed");
                        goto error;
                    }

                }
                else if (!idata) {
                    fprintf(stderr, "Error in mpool_alloc: %s\n", mpool_strerror(ret));
                    goto error;
                }
            }
            else {
                if (ilist_p->max_pages && ilist_p->pages_used >= ilist_p->max_pages) {
                    if (!ilist_p->head)
                        goto error;

                    idata           = ilist_p->head;
                    ilist_p->head   = ilist_p->head->next;


                    if (ilist_p->free_cb)
                        ilist_p->free_cb((char*)idata + sizeof(ilist_data_t) + ilist_p->key_len);


                    memset(&dbkey, 0, sizeof(DBT));

                    dbkey.ulen      = idata->key_len;
                    dbkey.flags     = DB_DBT_USERMEM;
                    dbkey.data      = idata->key;
                    dbkey.size      = idata->key_len;

                    ret = dbp->del(dbp, NULL, &dbkey, 0);
                    if (ret) {
                        dbp->err(dbp, ret, "db del failed");
                        goto error;
                    }
                }
                else {
                    idata = malloc(sizeof(ilist_data_t) + ilist_p->key_len + ilist_p->data_len);
                    ilist_p->pages_used++;

                    if (!idata) {
                        perror("malloc");
                        goto error;
                    }
                }
            }


            memset(idata, 0, sizeof(ilist_data_t));
            idata->key_len = key_len;
            memcpy(idata->key, key, key_len);

            if (!ilist_p->head)
                ilist_p->head       = idata;
            idata->prev         = ilist_p->end;
            if (ilist_p->end)
                ilist_p->end->next  = idata;
            ilist_p->end        = idata;




            val = (char*) idata;

            memset(&dbkey, 0, sizeof(DBT));
            memset(&dbdata, 0, sizeof(DBT));

            dbkey.ulen      = key_len;
            dbkey.flags     = DB_DBT_USERMEM;
            dbkey.data      = key;
            dbkey.size      = key_len;

            dbdata.ulen     = sizeof(val);
            dbdata.flags    = DB_DBT_USERMEM;
            dbdata.data     = &val;
            dbdata.size     = sizeof(val);

            ret = dbp->put(dbp, NULL, &dbkey, &dbdata, 0);
            if (ret) {
                dbp->err(dbp, ret, "db put failed");
                goto error;
            }


            *data = val + sizeof(ilist_data_t) + ilist_p->key_len;

            if (ilist_p->alloc_cb)
                ilist_p->alloc_cb(*data, ilist_p->data_len);

            return 0;

        }
        else
            return IL_NOT_FOUND;
    }
    else if (ret) {
        dbp->err(dbp, ret, "db get failed");
        goto error;
    }




    if (flags & IL_MOVE_TO_END) {
        ilist_data_t   *idata;


        idata = (ilist_data_t*) val;


        if (idata->next) {
            if (idata->prev)
                idata->prev->next   = idata->next;
            else
                ilist_p->head       = idata->next;
            idata->next->prev   = idata->prev;
            idata->prev         = ilist_p->end;
            if (ilist_p->end)
                ilist_p->end->next  = idata;
            idata->next         = NULL;
            ilist_p->end        = idata;
        }
        
    }


    *data = val + sizeof(ilist_data_t) + ilist_p->key_len;



    return 0;


error:

    return IL_ERROR;
}










int
ilist_del(ilist_t *ilist_p, char *key, size_t key_len, u_int32_t flags) {

    DB     *dbp;
    DBT     dbkey;
    DBT     dbdata;

    int ret;

    char   *val;




    dbp = ilist_p->dbp;



    memset(&dbkey, 0, sizeof(DBT));
    memset(&dbdata, 0, sizeof(DBT));

    dbkey.ulen      = key_len;
    dbkey.flags     = DB_DBT_USERMEM;
    dbkey.data      = key;
    dbkey.size      = key_len;

    dbdata.ulen     = sizeof(val);
    dbdata.flags    = DB_DBT_USERMEM;
    dbdata.data     = &val;


    ret = dbp->get(dbp, NULL, &dbkey, &dbdata, 0);
    if (ret == DB_NOTFOUND)
        return IL_NOT_FOUND;
    else if (ret) {
        dbp->err(dbp, ret, "db get failed");
        goto error;
    }





    {
        ilist_data_t   *idata;


        idata = (ilist_data_t*) val;


        if (idata->prev)
            idata->prev->next   = idata->next;
        else
            ilist_p->head       = idata->next;
        if (idata->next)
            idata->next->prev   = idata->prev;
        else
            ilist_p->end        = idata->prev;

    }




    if (ilist_p->free_cb)
        ilist_p->free_cb((char*)val + sizeof(ilist_data_t) + ilist_p->key_len);



    ret = dbp->del(dbp, NULL, &dbkey, 0);
    if (ret) {
        dbp->err(dbp, ret, "db del failed");
        goto error;
    }


    if (ilist_p->pool) {
        ret = mpool_free(ilist_p->pool, val, sizeof(ilist_data_t) + ilist_p->key_len + ilist_p->data_len);
        if (ret != MPOOL_ERROR_NONE) {
            fprintf(stderr, "Error in mpool_free: %s\n", mpool_strerror(ret));
            goto error;
        }
    }
    else {
        free(val);
        ilist_p->pages_used--;
    }




    return 0;


error:

    return IL_ERROR;
}








int
ilist_close(ilist_t *ilist_p) {

    int ret;
    ilist_data_t   *data;





    data = ilist_p->head;
    while(data) {
        ilist_data_t   *next;


        next = data->next;

        if (ilist_p->free_cb)
            ilist_p->free_cb((char*)data + sizeof(ilist_data_t) + ilist_p->key_len);

        if (!ilist_p->pool)
            free(data);
    }



    
    if (ilist_p->pool) {
        ret = mpool_close(ilist_p->pool);
        if (ret != MPOOL_ERROR_NONE) {
            fprintf(stderr, "Error in mpool_close: %s\n", mpool_strerror(ret));
        }
    }


    ilist_p->dbp->close(ilist_p->dbp, 0);


    free(ilist_p);


    return 0;
}





