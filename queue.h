#ifndef QUEUE_H
#define QUEUE_H

#include <mysql/mysql.h>

#include "ilist.h"


#include "config.h"





#define M_USER_ENTERED              "UserEntered"
#define M_USER_STATUS_CHANGED       "UserStatusChanged"
#define M_USER_LEFT                 "UserLeft"
#define M_CHAT_MESSAGE              "ChatMessage"
#define M_PRIVATE_MESSAGE           "PrivateMessage"
#define M_USER_NAME_CHANGED         "UserNameChanged"
#define M_START_TYPING              "StartTyping"
#define M_STOP_TYPING               "StopTyping"
#define M_FOLLOW_REQUEST            "FollowRequest"
#define M_FOLLOW_RESPONSE           "FollowResponse"
#define M_FOLLOW_REQUEST_CANCELLED  "FollowRequestCancelled"
#define M_FOLLOW_REQUEST_EXPIRED    "FollowRequestExpired"



typedef enum {
    Q_NONE = 0,
    Q_STARTTYPING = 1,
    Q_STOPTYPING = 2,
    Q_ACTIVE = 4
} queue_flags_e;

typedef u_int8_t queue_flags_t;



typedef enum {
    Q_ERROR = 1
} queue_error_e;

typedef u_int8_t queue_error_t;






typedef enum {
    S_AVAILABLE = 0,
    S_AWAY,
    S_AUTO_AWAY,
    /*S_RESERVED,*/
    S_DISCONNECTED = 4,
    S_UNKNOWN   = 255
} user_status_e;

typedef u_int8_t user_status_t;



typedef long auth_val_t;

struct user_s {
    struct user_s  *next;

    auth_val_t      id;
    user_status_t   status;
    auth_val_t      follow_user_id;
    int             follow_index;
};

typedef struct user_s user_t;





#define MESSAGETYPELEN  32

struct message_s {
    char            message_type[MESSAGETYPELEN];
    char            queue_id[QUEUEKEYLEN];
    auth_val_t      source_user_id;
    user_status_t   status;
    long            time;

    char    buf[MESSAGELEN];
};

typedef struct message_s message_t;



struct event_key_s {
    char        queue_id[QUEUEKEYLEN];
    auth_val_t  source_user_id;
};

typedef struct event_key_s event_key_t;


struct event_s {
    long    time;
};

typedef struct event_s event_t;

struct follow_event_s {
    long    time;

    char        response_queue_id[QUEUEKEYLEN];
    auth_val_t  dest_user_id;
};

typedef struct follow_event_s follow_event_t;



#define EVENTTYPENUM    3

typedef enum {
    E_AWAY = 0,
    E_DISCONNECTED,
    E_FOLLOWREQUEST
} event_type_e;

typedef u_int8_t event_type_t;





struct queue_s {
    size_t  n;

    user_t     *user_hash[USERHASHSIZE];

    message_t   messages[QUEUELEN];

};

typedef struct queue_s queue_t;




struct ahistory_in_s;

struct queue_mgm_s {
    ilist_t    *il;

    MYSQL      *mysql;
    MYSQL_STMT *insert_or_update_stmt;

    pthread_mutex_t     event_list_mutex;
    ilist_t    *event_list[EVENTTYPENUM];

    struct ahistory_in_s      *ain;
};

typedef struct queue_mgm_s queue_mgm_t;



int
queue_mgm_open(queue_mgm_t **qp, struct ahistory_in_s *ain, queue_flags_t flags);

int
queue_mgm_close(queue_mgm_t *qp);


int
queue_get(queue_mgm_t *qp, char *key, size_t key_len, auth_val_t a, int *n, message_t **begin, message_t **end, queue_flags_t flags);

int
queue_put(queue_mgm_t *qp, char *key, size_t key_len, int *n, message_t *m);


char *
message_get_value(message_t *m, char *name);

#endif
