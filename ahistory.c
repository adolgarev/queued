
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <pthread.h>

#include <arpa/inet.h>


#include "ahistory.h"
#include "config.h"


#define AH_INSERT_STMT "insert into " AH_TABLE "(messageType, time, sourceUserId, sourceUserName, destUserId, conversationId, text, newName, oldName, status, toChatRoomId, fromChatRoomId, queueId, _userAgent) select ?,?,?,name,?,?,?,?,?,?,?,?,?,? from " UH_TABLE " where userId=?"
#define AH_SELECT_CONVERSATION_STMT "select time, conversationId from " AH_TABLE " where messageType='PrivateMessage' and (sourceUserId=? and destUserId=? or sourceUserId=? and destUserId=?) order by time desc limit 1"
#define AH_UPDATE_CONVERSATION_STMT "INSERT INTO " CH_TABLE "(conversationId, firstUserId, secondUserId, startTime, endTime, firstMessageId, lastMessageId) VALUES(?,?,?,?,?,?,?) ON DUPLICATE KEY UPDATE endTime=VALUES(endTime), lastMessageId=VALUES(lastMessageId)"
#define AH_INSERT_PCONVERSATION_STMT "insert into " PCH_TABLE "(userId, queueId, startTime, firstMessageId) values(?,?,?,?)"
#define AH_SELECT_PCONVERSATION_STMT "select id from " PCH_TABLE " where userId=? and queueId=? and lastMessageId is null order by id desc limit 1"
#define AH_UPDATE_PCONVERSATION_STMT "update " PCH_TABLE " set endTime=?, lastMessageId=? where id=?"






#define SWAP_LONG_BYTES\
(x) ( \
      (((x) & 0x00000000000000ff) << 56) | \
      (((x) & 0x000000000000ff00) << 40) | \
      (((x) & 0x0000000000ff0000) << 24) | \
      (((x) & 0x00000000ff000000) <<  8) | \
      (((x) & 0x000000ff00000000) >>  8) | \
      (((x) & 0x0000ff0000000000) >> 24) | \
      (((x) & 0x00ff000000000000) >> 40) | \
      (((x) & 0xff00000000000000) >> 56)   \
    )

u_int64_t
htonll(u_int64_t hostlong) {
    if (htons(1) == 1)
        return hostlong;
    return SWAP_LONG_BYTES(hostlong);
}

/*u_int64_t
ntohll(u_int64_t networklong) {
    if (htons(1) == 1)
        return networklong;
    return SWAP_LONG_BYTES(networklong);
}*/








int
ahistory_init(ahistory_in_t *a) {

    int         ret;

    MYSQL      *mysql;
    MYSQL_STMT *stmt;


    memset(a, 0, sizeof(ahistory_in_t));


    /*mpool_t    *pool;
    pool = mpool_open(0, getpagesize(), NULL, &ret);
    if (pool == NULL) {
        fprintf(stderr, "Error in mpool_open: %s\n", mpool_strerror(ret));
        goto error;
    }
    a->pool = pool;*/



    ret = pthread_mutex_init(&a->list_mutex, NULL);
    if (ret) {
        fprintf(stderr, "Error in pthread_mutex_init\n");
        goto error;
    }





    if(!(mysql = mysql_init(NULL))) {
        printf("Failed to initate MySQL connection\n");
        goto error;
    }
    a->mysql = mysql;

    
    my_bool reconnect = 1;
    mysql_options(mysql, MYSQL_OPT_RECONNECT, &reconnect);



    mysql_options(mysql, MYSQL_SET_CHARSET_NAME, "utf8");


    
    if (!mysql_real_connect(mysql, AH_DBHOST, AH_DBUSER, AH_DBPASSWD, AH_DBDB, AH_DBPORT, AH_DBUNIXSOCKET, 0)) {
        printf("Failed to connect to MySQL: Error: %s\n", mysql_error(mysql));
        goto error;
    }


    /* DEBUG */
    /*{
        MYSQL_RES  *res;
        MYSQL_ROW   row;

        mysql_query(mysql, "show variables like 'character_set_%'");

        res = mysql_store_result(mysql);

        while((row = mysql_fetch_row(res)) != NULL) {
            printf(row[0]); printf("\t"); printf(row[1]);
            printf("\n");
        }

        mysql_free_result(res);
    }*/



    mysql_autocommit(mysql, 0);

    if (mysql_query(mysql, "SET SESSION TRANSACTION ISOLATION LEVEL READ COMMITTED")) {
        printf("Failed to set transaction isolation level: Error: %s\n", mysql_error(mysql));
        goto error;
    }


    if (!(stmt = mysql_stmt_init(mysql))) {
        fprintf(stderr, "mysql_stmt_init(), out of memory\n");
        goto error;
    }
    a->insert_stmt = stmt;
    if (mysql_stmt_prepare(stmt, AH_INSERT_STMT, strlen(AH_INSERT_STMT))) {
        fprintf(stderr, "%s\n", mysql_stmt_error(stmt));
        goto error;
    }


    if (!(stmt = mysql_stmt_init(mysql))) {
        fprintf(stderr, "mysql_stmt_init(), out of memory\n");
        goto error;
    }
    a->select_conversation_stmt = stmt;
    if (mysql_stmt_prepare(stmt, AH_SELECT_CONVERSATION_STMT, strlen(AH_SELECT_CONVERSATION_STMT))) {
        fprintf(stderr, "%s\n", mysql_stmt_error(stmt));
        goto error;
    }



    if (!(stmt = mysql_stmt_init(mysql))) {
        fprintf(stderr, "mysql_stmt_init(), out of memory\n");
        goto error;
    }
    a->update_conversation_stmt = stmt;
    if (mysql_stmt_prepare(stmt, AH_UPDATE_CONVERSATION_STMT, strlen(AH_UPDATE_CONVERSATION_STMT))) {
        fprintf(stderr, "%s\n", mysql_stmt_error(stmt));
        goto error;
    }






    if (!(stmt = mysql_stmt_init(mysql))) {
        fprintf(stderr, "mysql_stmt_init(), out of memory\n");
        goto error;
    }
    a->insert_public_conversation_stmt = stmt;
    if (mysql_stmt_prepare(stmt, AH_INSERT_PCONVERSATION_STMT, strlen(AH_INSERT_PCONVERSATION_STMT))) {
        fprintf(stderr, "%s\n", mysql_stmt_error(stmt));
        goto error;
    }

    if (!(stmt = mysql_stmt_init(mysql))) {
        fprintf(stderr, "mysql_stmt_init(), out of memory\n");
        goto error;
    }
    a->select_public_conversation_stmt = stmt;
    if (mysql_stmt_prepare(stmt, AH_SELECT_PCONVERSATION_STMT, strlen(AH_SELECT_PCONVERSATION_STMT))) {
        fprintf(stderr, "%s\n", mysql_stmt_error(stmt));
        goto error;
    }

    if (!(stmt = mysql_stmt_init(mysql))) {
        fprintf(stderr, "mysql_stmt_init(), out of memory\n");
        goto error;
    }
    a->update_public_conversation_stmt = stmt;
    if (mysql_stmt_prepare(stmt, AH_UPDATE_PCONVERSATION_STMT, strlen(AH_UPDATE_PCONVERSATION_STMT))) {
        fprintf(stderr, "%s\n", mysql_stmt_error(stmt));
        goto error;
    }









    a->curl = curl_easy_init();
    if (!a->curl)
        goto error;


    return 0;


error:

    return AH_ERROR;
}


int
ahistory_close(ahistory_in_t *a) {

    /*int ret;

    
    ret = mpool_close(a->pool);
    if (ret != MPOOL_ERROR_NONE) {
        fprintf(stderr, "Error in mpool_close: %s\n", mpool_strerror(ret));
    }*/


    pthread_mutex_destroy(&a->list_mutex);



    mysql_stmt_close(a->insert_stmt);

    mysql_stmt_close(a->select_conversation_stmt);

    mysql_stmt_close(a->update_conversation_stmt);

    mysql_stmt_close(a->insert_public_conversation_stmt);

    mysql_stmt_close(a->select_public_conversation_stmt);

    mysql_stmt_close(a->update_public_conversation_stmt);

    mysql_close(a->mysql);

    curl_easy_cleanup(a->curl);

    return 0;
}



int
ahistory_append(ahistory_in_t *a, message_t *m) {

    ahistory_list_t    *l;


    if (strcmp(m->message_type, M_CHAT_MESSAGE)
        && strcmp(m->message_type, M_PRIVATE_MESSAGE)
        && strcmp(m->message_type, M_USER_ENTERED)
        && strcmp(m->message_type, M_USER_STATUS_CHANGED)
        && strcmp(m->message_type, M_USER_LEFT)
        && strcmp(m->message_type, M_USER_NAME_CHANGED))
        return 0;


    /*int ret;
    l = mpool_alloc(a->pool, sizeof(ahistory_list_t), &ret);
    if (!l) {
        fprintf(stderr, "Error in mpool_alloc: %s\n", mpool_strerror(ret));
        goto error;
    }*/
    l = malloc(sizeof(ahistory_list_t));
    if (!l) {
        fprintf(stderr, "Error in malloc\n");
        goto error;
    }


    l->next = NULL;
    l->prev = NULL;
    memcpy(&l->data, m, sizeof(message_t));


    pthread_mutex_lock(&a->list_mutex);
    if (!a->head) {
        a->head = l;
        a->end  = l;
    }
    else {
        a->end->next = l;
        l->prev = a->end;
        a->end  = l;
    }
    pthread_mutex_unlock(&a->list_mutex);

    
    return 0;

error:

    return AH_ERROR;
}



int
ahistory_consume(ahistory_in_t *a, message_t *m) {

    ahistory_list_t    *l;



    pthread_mutex_lock(&a->list_mutex);
    if (a->head) {
        l       = a->head;
        a->head = a->head->next;

        if (a->head)
            a->head->prev = NULL;
        else
            a->end = NULL;
    }
    else {
        pthread_mutex_unlock(&a->list_mutex);
        return AH_NOT_FOUND;
    }
    pthread_mutex_unlock(&a->list_mutex);



    memcpy(m, &l->data, sizeof(message_t));



    /*int ret;
    ret = mpool_free(a->pool, l, sizeof(ahistory_list_t));
    if (ret != MPOOL_ERROR_NONE) {
        fprintf(stderr, "Error in mpool_free: %s\n", mpool_strerror(ret));
        goto error;
    }*/
    free(l);

    
    return 0;


/*error:

    return 1;*/
}





void*
ahistory(void *arg) {
    ahistory_in_t  *a;
    message_t       m;


    MYSQL          *mysql;



    int ret;


    a = arg;

    mysql = a->mysql;



    mysql_thread_init();





    ahistory_bind_t     types[] = {
        {MYSQL_TYPE_STRING,  0,  "messageType",      0l},
        {MYSQL_TYPE_LONGLONG,0,  "time",             0l},
        {MYSQL_TYPE_LONG,    0,  "sourceUserId",     0l},
        {MYSQL_TYPE_LONG,    0,  "destUserId",       0l},
        {MYSQL_TYPE_LONG,    0,  "conversationId",   0l},
        {MYSQL_TYPE_STRING,  0,  "text",             0l},
        {MYSQL_TYPE_STRING,  0,  "newName",          0l},
        {MYSQL_TYPE_STRING,  0,  "oldName",          0l},
        {MYSQL_TYPE_LONG,    0,  "status",           0l},
        {MYSQL_TYPE_LONG,    0,  "toChatRoomId",     0l},
        {MYSQL_TYPE_LONG,    0,  "fromChatRoomId",   0l},
        {MYSQL_TYPE_STRING,  0,  "queueId",          0l},
        {MYSQL_TYPE_STRING,  0,  "_userAgent",       0l},
        {MYSQL_TYPE_LONG,    0,  "sourceUserId",     0l}
    };


    int types_n = sizeof(types) / sizeof(ahistory_bind_t);





    while (1) {

        unsigned long mysql_tid;



        mysql_tid = mysql_thread_id(mysql);
        if (mysql_ping(mysql)) {
            printf("MySQL ping error: Error: %s\n", mysql_error(mysql));
            sleep(15);
            continue;
        }
        if (mysql_tid != mysql_thread_id(mysql)) {

            /* a reconnect occured, stmt was released */
            if (!(a->insert_stmt = mysql_stmt_init(mysql))) {
                fprintf(stderr, "mysql_stmt_init(), out of memory\n");
                goto error;
            }
            if (mysql_stmt_prepare(a->insert_stmt, AH_INSERT_STMT, strlen(AH_INSERT_STMT))) {
                fprintf(stderr, "%s\n", mysql_stmt_error(a->insert_stmt));
                goto error;
            }

            if (!(a->select_conversation_stmt = mysql_stmt_init(mysql))) {
                fprintf(stderr, "mysql_stmt_init(), out of memory\n");
                goto error;
            }
            if (mysql_stmt_prepare(a->select_conversation_stmt, AH_SELECT_CONVERSATION_STMT, strlen(AH_SELECT_CONVERSATION_STMT))) {
                fprintf(stderr, "%s\n", mysql_stmt_error(a->select_conversation_stmt));
                goto error;
            }

            if (!(a->update_conversation_stmt = mysql_stmt_init(mysql))) {
                fprintf(stderr, "mysql_stmt_init(), out of memory\n");
                goto error;
            }
            if (mysql_stmt_prepare(a->update_conversation_stmt, AH_UPDATE_CONVERSATION_STMT, strlen(AH_UPDATE_CONVERSATION_STMT))) {
                fprintf(stderr, "%s\n", mysql_stmt_error(a->update_conversation_stmt));
                goto error;
            }

            
            if (!(a->insert_public_conversation_stmt = mysql_stmt_init(mysql))) {
                fprintf(stderr, "mysql_stmt_init(), out of memory\n");
                goto error;
            }
            if (mysql_stmt_prepare(a->insert_public_conversation_stmt, AH_INSERT_PCONVERSATION_STMT, strlen(AH_INSERT_PCONVERSATION_STMT))) {
                fprintf(stderr, "%s\n", mysql_stmt_error(a->insert_public_conversation_stmt));
                goto error;
            }

            if (!(a->select_public_conversation_stmt = mysql_stmt_init(mysql))) {
                fprintf(stderr, "mysql_stmt_init(), out of memory\n");
                goto error;
            }
            if (mysql_stmt_prepare(a->select_public_conversation_stmt, AH_SELECT_PCONVERSATION_STMT, strlen(AH_SELECT_PCONVERSATION_STMT))) {
                fprintf(stderr, "%s\n", mysql_stmt_error(a->select_public_conversation_stmt));
                goto error;
            }

            if (!(a->update_public_conversation_stmt = mysql_stmt_init(mysql))) {
                fprintf(stderr, "mysql_stmt_init(), out of memory\n");
                goto error;
            }
            if (mysql_stmt_prepare(a->update_public_conversation_stmt, AH_UPDATE_PCONVERSATION_STMT, strlen(AH_UPDATE_PCONVERSATION_STMT))) {
                fprintf(stderr, "%s\n", mysql_stmt_error(a->update_public_conversation_stmt));
                goto error;
            }

        }




        ret = ahistory_consume(a, &m);
        if (ret == AH_NOT_FOUND) {
            sleep(1);
            continue;
        }
        else if (ret)
            break;



        if (mysql_query(mysql, "START TRANSACTION")) {
            printf("Failed to begin transaction: Error: %s\n", mysql_error(mysql));
            goto error;
        }


        while (1) {
            MYSQL_STMT     *stmt;
            my_bool         null;
            int             i;
            char           *p1;
            char           *p2;


            long            conversation_id;
            long            dest_user_id;
            long            last_message_id;



            MYSQL_BIND      bind[sizeof(types) / sizeof(ahistory_bind_t)];



            memset(bind, 0, sizeof(bind));


            stmt = a->insert_stmt;
            null = 1;




            conversation_id = 0;
            dest_user_id    = 0;

            if (!strcmp(m.message_type, M_PRIVATE_MESSAGE)) {
                MYSQL_STMT     *stmt;
                MYSQL_RES      *prepare_meta_result;


                my_bool         conversation_id_null;
                long            end_time;
                my_bool         end_time_null;



                char           *p;


                /* select conversation_id */
                MYSQL_BIND bind[4];
                MYSQL_BIND bind_r[2];



                stmt = a->select_conversation_stmt;

                memset(bind, 0, sizeof(bind));
                memset(bind_r, 0, sizeof(bind_r));


                prepare_meta_result = mysql_stmt_result_metadata(stmt);
                if (!prepare_meta_result) {
                    fprintf(stderr, " %s\n", mysql_stmt_error(stmt));
                    goto error;
                }

                p = message_get_value(&m, "destUserId");
                if (!p)
                    goto no_conversation;
                dest_user_id = strtoll(p, NULL, 10);



                bind[0].buffer_type     = MYSQL_TYPE_LONG;
                bind[0].buffer          = (char*)&m.source_user_id;
                bind[0].is_null         = 0;
                bind[0].length          = 0;

                bind[1].buffer_type     = MYSQL_TYPE_LONG;
                bind[1].buffer          = (char*)&dest_user_id;
                bind[1].is_null         = 0;
                bind[1].length          = 0;

                bind[2].buffer_type     = MYSQL_TYPE_LONG;
                bind[2].buffer          = (char*)&dest_user_id;
                bind[2].is_null         = 0;
                bind[2].length          = 0;

                bind[3].buffer_type     = MYSQL_TYPE_LONG;
                bind[3].buffer          = (char*)&m.source_user_id;
                bind[3].is_null         = 0;
                bind[3].length          = 0;



                if (mysql_stmt_bind_param(stmt, bind)) {
                    fprintf(stderr, "%s\n", mysql_stmt_error(stmt));
                    goto error;
                }

                if (mysql_stmt_execute(stmt)) {
                    fprintf(stderr, "%s\n", mysql_stmt_error(stmt));
                    goto error;
                }


                conversation_id_null    = 0;
                end_time_null           = 0;

                bind_r[0].buffer_type   = MYSQL_TYPE_LONGLONG;
                bind_r[0].buffer        = (char*)&end_time;
                bind_r[0].is_null       = &end_time_null;
                bind_r[0].length        = 0;

                bind_r[1].buffer_type   = MYSQL_TYPE_LONG;
                bind_r[1].buffer        = (char*)&conversation_id;
                bind_r[1].is_null       = &conversation_id_null;
                bind_r[1].length        = 0;


                if (mysql_stmt_bind_result(stmt, bind_r)) {
                    fprintf(stderr, " %s\n", mysql_stmt_error(stmt));
                    goto error;
                }

                /* buffer all results to client (optional step) */
                if (mysql_stmt_store_result(stmt)) {
                    fprintf(stderr, " %s\n", mysql_stmt_error(stmt));
                    goto error;
                }


                end_time        = 0;
                while (!mysql_stmt_fetch(stmt)) {
                    if (conversation_id_null)
                        conversation_id = 0;
                    if (end_time_null)
                        end_time        = 0;
                }


                mysql_free_result(prepare_meta_result);


                if (m.time - end_time > AH_CONV_INT)
                    conversation_id++;



            }

no_conversation:



            for (i = 0; i < types_n; i++) {
                bind[i].buffer_type = types[i].buffer_type;
                bind[i].is_null     = &null;

                if (!strcmp(types[i].name, "messageType")) {
                    types[i].len = strlen(m.message_type);
                    bind[i].buffer          = m.message_type;
                    bind[i].buffer_length   = types[i].len;
                    bind[i].is_null         = 0;
                    bind[i].length          = &types[i].len;
                }
                else if (!strcmp(types[i].name, "time")) {
                    types[i].long_val = m.time;
                    bind[i].buffer          = (char*)&types[i].long_val;
                    bind[i].is_null         = 0;
                    bind[i].length          = 0;
                }
                else if (!strcmp(types[i].name, "sourceUserId")) {
                    types[i].long_val = m.source_user_id;
                    bind[i].buffer          = (char*)&types[i].long_val;
                    bind[i].is_null         = 0;
                    bind[i].length          = 0;
                }
                else if (m.status != S_UNKNOWN && !strcmp(types[i].name, "status")) {
                    types[i].long_val = m.status;
                    bind[i].buffer          = (char*)&types[i].long_val;
                    bind[i].is_null         = 0;
                    bind[i].length          = 0;
                }
                else if (!strcmp(types[i].name, "queueId")) {
                    types[i].len = strlen(m.queue_id);
                    bind[i].buffer          = m.queue_id;
                    bind[i].buffer_length   = types[i].len;
                    bind[i].is_null         = 0;
                    bind[i].length          = &types[i].len;
                }
                else if (conversation_id && !strcmp(types[i].name, "conversationId")) {
                    bind[i].buffer          = (char*)&conversation_id;
                    bind[i].is_null         = 0;
                    bind[i].length          = 0;
                }

            }



            


            p1 = m.buf;


            while (*p1) {
                char   *endptr;

                p2 = p1 + strlen(p1) + 1;



                for (i = 0; i < types_n; i++) {
                    if (bind[i].is_null && !strcmp(p1, types[i].name)) {
                        if (bind[i].buffer_type == MYSQL_TYPE_STRING) {
                            types[i].len = strlen(p2);
                            bind[i].buffer          = p2;
                            bind[i].buffer_length   = types[i].len;
                            bind[i].is_null         = 0;
                            bind[i].length          = &types[i].len;
                        }
                        else if (bind[i].buffer_type == MYSQL_TYPE_LONG || bind[i].buffer_type == MYSQL_TYPE_LONGLONG) {
                            types[i].long_val = strtol(p2, &endptr, 10);
                            if (*endptr)
                                goto next;
                            bind[i].buffer          = (char*)&types[i].long_val;
                            bind[i].is_null         = 0;
                            bind[i].length          = 0;
                        }

                        break;
                    }

                }

next:
                p1 = p2 + strlen(p2) + 1;
            }





            if (mysql_stmt_bind_param(stmt, bind)) {
                fprintf(stderr, "%s\n", mysql_stmt_error(stmt));
                goto error;
            }

            if (mysql_stmt_execute(stmt)) {
                fprintf(stderr, "%s\n", mysql_stmt_error(stmt));
                goto error;
            }

            last_message_id     = mysql_insert_id(mysql);


#ifdef AH_INDEXURL
            if (!strcmp(m.message_type, M_CHAT_MESSAGE) || !strcmp(m.message_type, M_PRIVATE_MESSAGE)) {
                CURL           *curl;
                addmessage_t    am;
                char           *text;


                curl = a->curl;

                memset(&am, 0, sizeof(am));

                am.id   = htonl(mysql_insert_id(a->mysql));
                am.time = htonll(m.time);
                strcpy(am.message_type, m.message_type);
                strcpy(am.queue_id, m.queue_id);
                text    = message_get_value(&m, "text");
                if (text)
                    strcpy(am.buf, text);

                curl_easy_setopt(curl, CURLOPT_URL, AH_INDEXURL);
                curl_easy_setopt(curl, CURLOPT_POSTFIELDS, &am);
                curl_easy_setopt(curl, CURLOPT_POSTFIELDSIZE, sizeof(am));
                curl_easy_perform(curl);
            }
#endif

            if (!strcmp(m.message_type, M_USER_ENTERED)) {
                MYSQL_STMT     *stmt;
                MYSQL_BIND bind[4];

                unsigned long      queue_id_len;




                /* insert into public_conversation_history */

                stmt = a->insert_public_conversation_stmt;

                memset(bind, 0, sizeof(bind));


                bind[0].buffer_type     = MYSQL_TYPE_LONG;
                bind[0].buffer          = (char*)&m.source_user_id;
                bind[0].is_null         = 0;
                bind[0].length          = 0;

                queue_id_len            = strlen(m.queue_id);
                bind[1].buffer_type     = MYSQL_TYPE_STRING;
                bind[1].buffer_length   = queue_id_len;
                bind[1].buffer          = m.queue_id;
                bind[1].is_null         = 0;
                bind[1].length          = &queue_id_len;

                bind[2].buffer_type     = MYSQL_TYPE_LONGLONG;
                bind[2].buffer          = (char*)&m.time;
                bind[2].is_null         = 0;
                bind[2].length          = 0;

                bind[3].buffer_type     = MYSQL_TYPE_LONG;
                bind[3].buffer          = (char*)&last_message_id;
                bind[3].is_null         = 0;
                bind[3].length          = 0;


                if (mysql_stmt_bind_param(stmt, bind)) {
                    fprintf(stderr, "%s\n", mysql_stmt_error(stmt));
                    goto error;
                }

                if (mysql_stmt_execute(stmt)) {
                    fprintf(stderr, "%s\n", mysql_stmt_error(stmt));
                    goto error;
                }
            }
            else if (!strcmp(m.message_type, M_USER_LEFT)) {
                MYSQL_STMT     *stmt;
                MYSQL_RES      *prepare_meta_result;


                my_bool         public_conversation_id_null;
                long            public_conversation_id;


                unsigned long      queue_id_len;




                /* select public_conversation_id */
                MYSQL_BIND bind[2];
                MYSQL_BIND bind_r[1];



                stmt = a->select_public_conversation_stmt;

                memset(bind, 0, sizeof(bind));
                memset(bind_r, 0, sizeof(bind_r));


                prepare_meta_result = mysql_stmt_result_metadata(stmt);
                if (!prepare_meta_result) {
                    fprintf(stderr, " %s\n", mysql_stmt_error(stmt));
                    goto error;
                }



                bind[0].buffer_type     = MYSQL_TYPE_LONG;
                bind[0].buffer          = (char*)&m.source_user_id;
                bind[0].is_null         = 0;
                bind[0].length          = 0;

                queue_id_len            = strlen(m.queue_id);
                bind[1].buffer_type     = MYSQL_TYPE_STRING;
                bind[1].buffer_length   = queue_id_len;
                bind[1].buffer          = m.queue_id;
                bind[1].is_null         = 0;
                bind[1].length          = &queue_id_len;



                if (mysql_stmt_bind_param(stmt, bind)) {
                    fprintf(stderr, "%s\n", mysql_stmt_error(stmt));
                    goto error;
                }

                if (mysql_stmt_execute(stmt)) {
                    fprintf(stderr, "%s\n", mysql_stmt_error(stmt));
                    goto error;
                }


                public_conversation_id_null    = 0;

                bind_r[0].buffer_type   = MYSQL_TYPE_LONG;
                bind_r[0].buffer        = (char*)&public_conversation_id;
                bind_r[0].is_null       = &public_conversation_id_null;
                bind_r[0].length        = 0;


                if (mysql_stmt_bind_result(stmt, bind_r)) {
                    fprintf(stderr, " %s\n", mysql_stmt_error(stmt));
                    goto error;
                }

                /* buffer all results to client (optional step) */
                if (mysql_stmt_store_result(stmt)) {
                    fprintf(stderr, " %s\n", mysql_stmt_error(stmt));
                    goto error;
                }


                public_conversation_id = 0;
                while (!mysql_stmt_fetch(stmt)) {
                    if (public_conversation_id_null)
                        public_conversation_id = 0;
                }


                mysql_free_result(prepare_meta_result);

                
                if (public_conversation_id) {
                    MYSQL_STMT     *stmt;
                    MYSQL_BIND bind[3];




                    /* insert into public_conversation_history */

                    stmt = a->update_public_conversation_stmt;

                    memset(bind, 0, sizeof(bind));


                    bind[0].buffer_type     = MYSQL_TYPE_LONGLONG;
                    bind[0].buffer          = (char*)&m.time;
                    bind[0].is_null         = 0;
                    bind[0].length          = 0;

                    bind[1].buffer_type     = MYSQL_TYPE_LONG;
                    bind[1].buffer          = (char*)&last_message_id;
                    bind[1].is_null         = 0;
                    bind[1].length          = 0;

                    bind[2].buffer_type     = MYSQL_TYPE_LONG;
                    bind[2].buffer          = (char*)&public_conversation_id;
                    bind[2].is_null         = 0;
                    bind[2].length          = 0;


                    if (mysql_stmt_bind_param(stmt, bind)) {
                        fprintf(stderr, "%s\n", mysql_stmt_error(stmt));
                        goto error;
                    }

                    if (mysql_stmt_execute(stmt)) {
                        fprintf(stderr, "%s\n", mysql_stmt_error(stmt));
                        goto error;
                    }

                }

            }



            if (conversation_id) {
                MYSQL_STMT     *stmt;
                MYSQL_BIND bind[7];



                /* insert or update conversation_history */

                stmt = a->update_conversation_stmt;

                memset(bind, 0, sizeof(bind));


                bind[0].buffer_type     = MYSQL_TYPE_LONG;
                bind[0].buffer          = (char*)&conversation_id;
                bind[0].is_null         = 0;
                bind[0].length          = 0;

                bind[1].buffer_type     = MYSQL_TYPE_LONG;
                bind[1].buffer          = m.source_user_id < dest_user_id ? (char*)&m.source_user_id : (char*)&dest_user_id;
                bind[1].is_null         = 0;
                bind[1].length          = 0;

                bind[2].buffer_type     = MYSQL_TYPE_LONG;
                bind[2].buffer          = m.source_user_id > dest_user_id ? (char*)&m.source_user_id : (char*)&dest_user_id;
                bind[2].is_null         = 0;
                bind[2].length          = 0;

                bind[3].buffer_type     = MYSQL_TYPE_LONGLONG;
                bind[3].buffer          = (char*)&m.time;
                bind[3].is_null         = 0;
                bind[3].length          = 0;

                bind[4].buffer_type     = MYSQL_TYPE_LONGLONG;
                bind[4].buffer          = (char*)&m.time;
                bind[4].is_null         = 0;
                bind[4].length          = 0;

                bind[5].buffer_type     = MYSQL_TYPE_LONG;
                bind[5].buffer          = (char*)&last_message_id;
                bind[5].is_null         = 0;
                bind[5].length          = 0;

                bind[6].buffer_type     = MYSQL_TYPE_LONG;
                bind[6].buffer          = (char*)&last_message_id;
                bind[6].is_null         = 0;
                bind[6].length          = 0;


                if (mysql_stmt_bind_param(stmt, bind)) {
                    fprintf(stderr, "%s\n", mysql_stmt_error(stmt));
                    goto error;
                }

                if (mysql_stmt_execute(stmt)) {
                    fprintf(stderr, "%s\n", mysql_stmt_error(stmt));
                    goto error;
                }
            }



            ret = ahistory_consume(a, &m);
            if (ret == AH_NOT_FOUND)
                break;
            else if (ret)
                break;

        }
        
        if (mysql_query(mysql, "COMMIT")) {
            printf("Failed to commit transaction: Error: %s\n", mysql_error(mysql));
            goto error;
        }


        sleep(1);

        continue;

    


error:
        
        if (mysql_query(mysql, "ROLLBACK")) {
            printf("Failed to rollback transaction: Error: %s\n", mysql_error(mysql));
            goto error;
        }

    }


    mysql_thread_end();




    return NULL;
}




#if 0
int
ahistory_init(ahistory_in_t *a) {


    DB_ENV *db_env;
    DB     *dbp;
    int     ret;


    memset(a, 0, sizeof(ahistory_in_t));



    ret = db_env_create(&db_env, 0);
    if (ret) {
        fprintf(stderr, "Error creating env handle: %s\n", db_strerror(ret));
        goto error;
    }


    db_env->set_errfile(db_env, stderr);
    db_env->set_errpfx(db_env, "ahistory");

    ret = db_env->open(db_env,
            "testEnv",
            DB_CREATE | DB_INIT_MPOOL | DB_THREAD,
            0);
    if (ret) {
        db_env->err(db_env, ret, "%s: environment open failed", "ahistory");
        goto error;
    }


    ret = db_create(&dbp, db_env, 0);
    if (ret) {
        fprintf(stderr, "Error creating db handle: %s\n", db_strerror(ret));
        goto error;
    }


    dbp->set_errfile(dbp, stderr);
    dbp->set_errpfx(dbp, "ahistory");


    ret = dbp->set_re_len(dbp, MESSAGELEN);
    if (ret) {
        dbp->err(dbp, ret, "%s: set_re_len failed", "ahistory");
        goto error;
    }

    ret = dbp->set_q_extentsize(dbp, 100000);
    if (ret) {
        dbp->err(dbp, ret, "%s: set_q_extentsize failed", "ahistory");
        goto error;
    }

    ret = dbp->set_flags(dbp, DB_INORDER);
    if (ret) {
        dbp->err(dbp, ret, "%s: set_flags failed", "ahistory");
        goto error;
    }


    ret = dbp->open(dbp,
            NULL,
            "testDb.db",
            NULL,
            DB_QUEUE,
            DB_CREATE,
            0);
    if (ret != 0) {
        dbp->err(dbp, ret, "%s: db open failed", "ahistory");
        goto error;
    }



    a->db_env   = db_env;
    a->dbp      = dbp;



    return 0;

error:

    return 1;
}

int
ahistory_close(ahistory_in_t *a) {

    DB_ENV *db_env;
    DB     *dbp;

    db_env  = a->db_env;
    dbp     = a->dbp;

    if (dbp)
        dbp->close(dbp, 0);

    if (db_env)
        db_env->close(db_env, 0);

    return 0;
}
#endif


