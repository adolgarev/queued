
#include <pthread.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/stat.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <sys/time.h>
#include <fcntl.h>


#include <curl/curl.h>

#include <fcgi_config.h>
#include <fcgiapp.h>



#include "ConvertUTF.h"

#include "ilist.h"
#include "queue.h"
#include "ahistory.h"
#include "config.h"



#define THREAD_COUNT    0



static int              sock;
static ilist_t         *auth_list;
static queue_mgm_t     *queue;
static ahistory_in_t    ahistory_in;












int hexToChar[0x100][0x100];

void
init_escape() {
    int i, j;
    char str[3];

    for (i = 0; i < 0x100; i++) {
        for (j = 0; j < 0x100; j++) {
            if (!isxdigit(i) || !isxdigit(j))
                hexToChar[i][j] = 0;
            else {
                str[0] = (char) i;
                str[1] = (char) j;
                str[2] = 0;
                hexToChar[i][j] = strtol(str, NULL, 16);
            }
        }
    }
}












static void
perror_fcgi(FCGX_Stream *out, FCGX_Stream *err, const char *s, const char *e) {

    FCGX_FPrintF(out,
            "Status: 500 Server Error\r\n"
            "Content-Type: text/plain\r\n\r\n"
            "%s: %s\n",
            s,
            e);
    FCGX_FPrintF(err,
            "%s: %s\n",
            s,
            e);
}











void
utoa(char **src, UTF16 c) {

    char   *p;

    p = *src;

    if (c >= 10000) {
        *p++ = c / 10000 + '0';
        c -= c / 10000 * 10000;
        *p++ = c / 1000 + '0';
        c -= c / 1000 * 1000;
        *p++ = c / 100 + '0';
        c -= c / 100 * 100;
        *p++ = c / 10 + '0';
        c -= c / 10 * 10;
    }
    else if (c >= 1000) {
        *p++ = c / 1000 + '0';
        c -= c / 1000 * 1000;
        *p++ = c / 100 + '0';
        c -= c / 100 * 100;
        *p++ = c / 10 + '0';
        c -= c / 10 * 10;
    }
    else if (c >= 100) {
        *p++ = c / 100 + '0';
        c -= c / 100 * 100;
        *p++ = c / 10 + '0';
        c -= c / 10 * 10;
    }
    else if (c >= 10) {
        *p++ = c / 10 + '0';
        c -= c / 10 * 10;
    }
    *p++ = c + '0';

    *src = p;

}






void
strtov(char *nptr, char **endptr, char *dest, size_t dest_len) {
    char   *p1;
    char   *p2;

    p1 = nptr;
    p2 = dest;
    while (1) {

        unsigned char c = *p1++;

        if (c == '&' || c == '\0') {
            if (p2 < dest + dest_len)
                *p2++ = '\0';
            if (endptr)
                *endptr = p1 - 1;
            break;
        }
        else if (c == '+') {
            if (p2 < dest + dest_len)
                *p2++ = ' ';
        }
        else if (c == '%') {
            int d1 = *p1++;
            int d2 = *p1++;
            unsigned char c2 = hexToChar[d1][d2];
            if (c2) {
                if (p2 < dest + dest_len)
                    *p2++ = c2;
            }
            else {
                if (p2 < dest + dest_len)
                    *p2++ = c;
                p1 -= 2;
            }

        }
        else {
            if (p2 < dest + dest_len)
                *p2++ = c;
        }

    }
    
}




static void *doit(void *a) {
    int rc;
    FCGX_Request    req;

    char   *query_string;
    char   *document_uri;
    char   *cookie;


    FCGX_InitRequest(&req, sock, 0);

    while (1) {
        static pthread_mutex_t accept_mutex = PTHREAD_MUTEX_INITIALIZER;

        /* Some platforms require accept() serialization, some don't.. */
        pthread_mutex_lock(&accept_mutex);
        rc = FCGX_Accept_r(&req);
        pthread_mutex_unlock(&accept_mutex);

        if (rc < 0)
            break;


        query_string    = FCGX_GetParam("QUERY_STRING", req.envp);
        document_uri    = FCGX_GetParam("DOCUMENT_URI", req.envp);
        cookie          = FCGX_GetParam("HTTP_COOKIE", req.envp);








        /* authentication */
        char       *auth_key;
        size_t      auth_key_len;
        auth_val_t *auth_val;


#if 1
        {
            char   *pos;
            int     ret;


            if (!cookie)
                goto auth_failed;

            auth_key = strstr(cookie, AUTH_KEY_NAME);
            if (!auth_key)
                goto auth_failed;

            auth_key += strlen(AUTH_KEY_NAME) + 1;
            auth_key_len = 0;
            for (pos = auth_key; *pos != '\0' && *pos != ';' && auth_key_len < AUTH_KEY_LEN; pos++)
                auth_key_len++;


            ret = ilist_get(auth_list, auth_key, auth_key_len, (char**)&auth_val, IL_MOVE_TO_END);
            if (ret == IL_NOT_FOUND)
                auth_val = NULL;
            else if (ret)
                goto auth_failed;


            goto auth_end;

        auth_failed:
            auth_val = NULL;

        auth_end:
            ;
        }
#else
        /* DEBUG */
        auth_key                = "BBBB";
        auth_key_len            = 4;
        auth_val_t auth_vall    = 5;
        auth_val                = &auth_vall;
#endif



        if (!strcmp(document_uri, "/sendMessage")) {
            message_t   m;
            char       *p;
            char       *q;
            size_t      query_string_len;
            char       *param_name;

            char       *request_id_p;
            char        request_id[REQUESTIDLEN];

            request_id_p = strstr(query_string, "requestId");
            if (request_id_p)
                strtov(request_id_p + 10, NULL, request_id, REQUESTIDLEN);
            


            query_string_len = strlen(query_string);


            /* encode message */
            p = m.buf;
            /* remove requestId from message, assumed that requestId is the first parameter */
            if (request_id_p) {
                q = strchr(query_string, '&');
                if (!q)
                    q = query_string + query_string_len;
                else
                    q++;
            }
            else
                q = query_string;

            memset(&m, 0, sizeof(message_t));
            m.status = S_UNKNOWN;



            {
                struct timeval tv;

                gettimeofday(&tv, NULL);
                m.time = tv.tv_sec * 1000 + tv.tv_usec / 1000;
            }


            param_name = m.buf;
            while (p < m.buf + MESSAGELEN && q < query_string + query_string_len) {
                
                unsigned char c = *q++;

                /* TODO: name without value */
                if (c == '&') {
                    *p++ = '\0';
                    param_name = p;
                }
                else if (c == '=') {
                    *p++ = '\0';

                    if (!strcmp(param_name, "messageType"))
                        strtov(q, &q, m.message_type, MESSAGETYPELEN);
                    else if (!strcmp(param_name, "queueId"))
                        strtov(q, &q, m.queue_id, QUEUEKEYLEN);
                    else if (!strcmp(param_name, "sourceUserId"))
                        m.source_user_id = strtol(q, &q, 10);
                    else if (!strcmp(param_name, "status"))
                        m.status = strtol(q, &q, 10);
                    else
                        continue;

                    p = param_name;
                    q++;
                }
                else if (c == '+')
                    *p++ = ' ';
                else if (c == '%' && query_string + query_string_len - q >= 2) {
                    int d1 = *q++;
                    int d2 = *q++;
                    unsigned char c2 = hexToChar[d1][d2];
                    if (c2)
                        *p++ = c2;
                    else {
                        *p++ = c;
                        q -= 2;
                    }

                }
                else
                    *p++ = c;
            }
            *p++ = '\0';



            /* update auth_list if user is not authenticated or user enters chat room */
            if (!auth_val || !strcmp(m.message_type, M_USER_ENTERED)) {

                int ret;

                if (strcmp(m.message_type, M_USER_ENTERED))
                    goto not_logged;

                if (!m.source_user_id)
                    goto not_logged;


                ret = ilist_get(auth_list, auth_key, auth_key_len, (char**)&auth_val, IL_CREATE);
                if (ret)
                    goto not_logged;
                *auth_val = m.source_user_id;


                goto logged;


            not_logged:
                perror_fcgi(req.out, req.err, "User Not Logged", "you must login first");
                goto end;

            logged:
                ;
            }


            /* test auth_val */
            {

                if (!m.source_user_id)
                    m.source_user_id = *auth_val;
                else {

                    if (m.source_user_id != *auth_val) {
                        perror_fcgi(req.out, req.err, "User Not Logged", "wrong sourceUserId");
                        goto end;
                    }
                }
            }





            /* send message */
            int         n;
            {

                if (*m.queue_id) {
                    if (queue_put(queue, m.queue_id, strlen(m.queue_id), &n, &m)) {
                        perror_fcgi(req.out, req.err, "Server Error", "cannot send message");
                        goto end;
                    }
                }
                else {
                    perror_fcgi(req.out, req.err, "Server Error", "queue is not specified");
                    goto end;
                }
            }


            if (request_id_p) {
                FCGX_FPrintF(req.out, "Content-type: text/javascript\r\n\r\n");
                FCGX_FPrintF(req.out, JSCALLBACK "('%s', '%d');", request_id, n);
            }
            else
                FCGX_FPrintF(req.out, "Content-type: text/plain\r\n\r\n%d", n);


        }
        else if (!strcmp(document_uri, "/getMessages")) {


            if (!auth_val) {
                perror_fcgi(req.out, req.err, "User Not Logged", "you must login first");
                goto end;
            }


            message_t      *begin;
            message_t      *end;
            queue_flags_t   flags;


            char    queue_id[QUEUEKEYLEN];
            char    request_id[REQUESTIDLEN];
            int     message_number;
            unsigned short  print_comma1;


            char       *q;
            char       *param_name;


            flags = 0;
            memset(queue_id, 0, QUEUEKEYLEN);
            memset(request_id, 0, REQUESTIDLEN);
            message_number = -2;
            print_comma1 = 0;

            param_name = strstr(query_string, "requestId");
            if (param_name)
                strtov(param_name + 10, NULL, request_id, REQUESTIDLEN);
            

            FCGX_FPrintF(req.out, "Content-type: text/javascript\r\n\r\n");

            if (*request_id)
                FCGX_FPrintF(req.out, JSCALLBACK "('%s', '[", request_id);
            else
                FCGX_FPrintF(req.out, "[");


            /* decode request */
            q = query_string;
            param_name = query_string;



            while (1) {
                
                unsigned char c = *q++;

                if (c == '=') {
                    if (!strncmp(param_name, "queueId", 7))
                        strtov(q, &q, queue_id, QUEUEKEYLEN);
                    else if (!strncmp(param_name, "messageNumber", 13))
                        message_number = strtol(q, &q, 10);
                }
                else if (c == '&' || c == '\0') {

                    if (!strncmp(param_name, "startTyping", 11))
                        flags |= Q_STARTTYPING;
                    else if (!strncmp(param_name, "stopTyping", 10))
                        flags |= Q_STOPTYPING;
                    else if (!strncmp(param_name, "active", 6))
                        flags |= Q_ACTIVE;

                    if (c == '&')
                        param_name = q;

                    if (*queue_id && message_number >= -2 && (c == '\0' || *q == 'q')) {
                        unsigned short print_comma2;

                        unsigned short get_all_messages;
                        message_t      *begin1;
                        message_t      *end1;

                        /* get messages from specified queue */

                        if (message_number == -2) {
                            get_all_messages    = 1;
                            message_number      = 0;
                        }
                        else
                            get_all_messages    = 0;

                        if (queue_get(queue, queue_id, strlen(queue_id), *auth_val, &message_number, &begin, &end, flags)) {
                            perror_fcgi(req.out, req.err, "Server Error", "cannot get messages");
                            goto end;
                        }

                        
                        if (get_all_messages) {
                            begin1  = begin;
                            end1    = end;

                            begin   = end1;
                            end     = begin1 + QUEUELEN;
                        }


                        if (print_comma1)
                            FCGX_FPrintF(req.out, ",");
                        else
                            print_comma1 = 1;
                        FCGX_FPrintF(req.out, "{\"queueId\":\"%s\",\"messageNumber\":%d,\"messages\":[", queue_id, message_number);

                        print_comma2 = 0;
                        while (begin && end && (begin < end || get_all_messages)) {
                            char *p1;
                            char *p2;


                            if (get_all_messages && begin == end) {
                                begin   = begin1;
                                end     = end1;
                                get_all_messages = 0;
                                continue;
                            }

                            if (!*begin->message_type) {
                                begin++;
                                continue;
                            }



                            p1 = begin->buf;

                            if (print_comma2)
                                FCGX_FPrintF(req.out, ",");
                            else
                                print_comma2 = 1;
                            FCGX_FPrintF(req.out, "{");

                            FCGX_FPrintF(req.out, "\"messageType\":\"%s\",\"sourceUserId\":\"%ld\",\"time\":\"%ld\"",
                                    begin->message_type, begin->source_user_id, begin->time);
                            if (begin->status != S_UNKNOWN)
                                FCGX_FPrintF(req.out, ",\"status\":\"%u\"", begin->status);


                            while (*p1) {

                                p2 = p1 + strlen(p1) + 1;


                                /* encode p2 */
                                if (*p1 != '_') {   /* do not pass attributes with leading '_' */
                                    UTF16   buf[MESSAGELEN*4];
                                    UTF16  *dest_start;
                                    UTF16  *dest_end;
                                    const UTF8   *src_start;
                                    const UTF8   *src_end;

                                    char    buf1[MESSAGELEN*4];
                                    char   *p;

                                    dest_start  = buf;
                                    dest_end    = buf + sizeof(buf);
                                    src_start   = (UTF8*)p2;
                                    src_end     = (UTF8*)(p2 + strlen(p2));


                                    ConvertUTF8toUTF16(&src_start, src_end, &dest_start, dest_end, lenientConversion);

                                    dest_end    = dest_start;
                                    dest_start  = buf;
                                    p           = buf1;
                                    while (dest_start < dest_end && p < buf1 + sizeof(buf1)) {
                                        UTF16 c = *dest_start;
                                        if (c < 128 && (isalnum(c) || c == '.'))
                                            *p++ = c;
                                        else {
                                            /*sprintf(p, "&#%u;", c);*/
                                            *p++ = '&';
                                            *p++ = '#';
                                            utoa(&p, c);
                                            *p++ = ';';
                                            /*p += strlen(p);*/
                                        }

                                        dest_start++;
                                    }
                                    *p = '\0';

                                    FCGX_FPrintF(req.out, ",\"%s\":\"%s\"", p1, buf1);
                                    p1 = (char*)src_end + 1;
                                }
                                else {
                                    p1 = p2 + strlen(p2) + 1;
                                }

                                
                                /*p1 = p2 + strlen(p2) + 1;*/
                            }

                            FCGX_FPrintF(req.out, "}");


                            begin++;
                        }

                        FCGX_FPrintF(req.out, "]}");


                        memset(queue_id, 0, QUEUEKEYLEN);
                        message_number = -2;
                        flags = 0;
                    }

                    if (c == '\0')
                        break;
                }
            }

            if (*request_id)
                FCGX_FPrintF(req.out, "]');");
            else
                FCGX_FPrintF(req.out, "]");

        }
        else
            perror_fcgi(req.out, req.err, "Server Error: unknown DOCUMENT_URI", document_uri);










        /*FCGX_FPrintF(req.out, "Content-type: text/plain\r\n\r\n");*/
end:

        FCGX_Finish_r(&req);
    }

    return NULL;
}



int main(void) {
    int i;
    pthread_t   tid[THREAD_COUNT];

    pthread_t       ahistory_tid;





#if 1
    /* run as daemon */
    i = fork();
    if (i < 0) exit(1); /* fork error */
    if (i > 0) exit(0); /* parent exits */
    /* child (daemon) continues */

    setsid(); /* obtain a new process group */

    for (i = getdtablesize(); i >= 0; --i) close(i); /* close all descriptors */
    i = open("/dev/null", O_RDWR); /* open stdin */
    dup(i); /* stdout */
    dup(i); /* stderr */

    /* file creation Mask */
    umask(027);

    /* running directory */
    chdir("/");
#endif




    curl_global_init(CURL_GLOBAL_ALL);



    FCGX_Init();

    init_escape();


#ifdef UNIX_SOCKET
    int old_umask;
    int access_mask;
    access_mask = 0766;
    old_umask = umask(~(access_mask & 0777));
    sock = FCGX_OpenSocket(UNIX_SOCKET, BACKLOG);
    umask(old_umask);
#else
    sock = FCGX_OpenSocket(SOCKET, BACKLOG);
#endif


    /*if (ilist_open(&auth_list, AUTH_KEY_LEN, sizeof(auth_val_t), getpagesize(), sizeof(auth_val_t) * AUTH_MAP_SIZE / getpagesize(), NULL, NULL, 0))*/
    if (ilist_open(&auth_list, AUTH_KEY_LEN, sizeof(auth_val_t), 0, AUTH_MAP_SIZE, NULL, NULL, IL_NOPOOL))
        goto error;





    if (ahistory_init(&ahistory_in))
        goto error;

    pthread_create(&ahistory_tid, NULL, ahistory, &ahistory_in);




    if (queue_mgm_open(&queue, &ahistory_in, 0))
        goto error;


    /* DEBUG */
    /*{
        char        auth_key[AUTH_KEY_LEN];
        auth_val_t *auth_val;

        memset(auth_key, 0, AUTH_KEY_LEN);
        strcpy(auth_key, "D93FCD858E79C223ED3FC7428C5AEEF1");
        ilist_get(auth_list, auth_key, strlen(auth_key), (char**)&auth_val, IL_CREATE);
        *auth_val = 1000;
    }*/


    for (i = 1; i < THREAD_COUNT; i++)
        pthread_create(&tid[i], NULL, doit, NULL);






    doit(0);



    ilist_close(auth_list);


    if (queue_mgm_close(queue))
        goto error;



    ahistory_close(&ahistory_in);



    curl_global_cleanup();

    return 0;

error:

    curl_global_cleanup();

    return 1;
}

