#ifndef ILIST_H
#define ILIST_H


#include <db.h>

#include <mpool.h>


#include "config.h"





typedef enum {
    IL_NONE = 0,
    IL_CREATE = 1,
    IL_MOVE_TO_END = 2,
    IL_NOPOOL = 4
} ilist_flags_e;

typedef u_int32_t ilist_flags_t;



typedef enum {
    IL_ERROR = 1,
    IL_NOT_FOUND
} ilist_error_e;

typedef u_int8_t ilist_error_t;






struct ilist_data_s {
    struct ilist_data_s    *next;
    struct ilist_data_s    *prev;

    size_t  key_len;
    char    key[0];
};

typedef struct ilist_data_s ilist_data_t;





typedef void (*ilist_alloc_cb)(char *data, size_t data_len);

typedef void (*ilist_free_cb)(char *data);


struct ilist_s {
    ilist_data_t   *head;
    ilist_data_t   *end;

    size_t  key_len;
    size_t  data_len;


    int     page_size;
    size_t  max_pages;
    size_t  pages_used;


    DB         *dbp;
    mpool_t    *pool;

    ilist_alloc_cb  alloc_cb;
    ilist_free_cb   free_cb;
};

typedef struct ilist_s ilist_t;




int
ilist_open(ilist_t **ilist_p, size_t key_len, size_t data_len, int page_size, size_t max_pages, ilist_alloc_cb alloc_cb, ilist_free_cb free_cb, u_int32_t flags);

int
ilist_get(ilist_t *ilist_p, char *key, size_t key_len, char **data, u_int32_t flags);

int
ilist_del(ilist_t *ilist_p, char *key, size_t key_len, u_int32_t flags);

int
ilist_close(ilist_t *ilist_p);

#endif
