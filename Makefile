.PHONY : clean

CFLAGS = -g -Wall -I./mpool-2.1.0 -L./mpool-2.1.0


all: mpool queue_fcgi test


mpool:
	cd mpool-2.1.0 && $(MAKE)


test.o: test.c ilist.h queue.h
	$(CC) $(CFLAGS) -c $<

ilist.o: ilist.c ilist.h config.h
	$(CC) $(CFLAGS) -c $<

queue.o: queue.c queue.h ilist.h config.h
	$(CC) $(CFLAGS) -c $<

ahistory.o: ahistory.c ahistory.h config.h
	$(CC) $(CFLAGS) -c $<

test: test.o ilist.o queue.o ahistory.o
	$(CC) $(CFLAGS) $^ -o $@ -ldb -lmpool -lmysqlclient_r -lcurl

ConvertUTF.o: ConvertUTF.c ConvertUTF.h
	$(CC) $(CFLAGS) -c $<

queue_fcgi.o: queue_fcgi.c queue.h ilist.h config.h ConvertUTF.h ahistory.h
	$(CC) $(CFLAGS) -c $<

queue_fcgi: queue_fcgi.o ilist.o queue.o ConvertUTF.o ahistory.o
	$(CC) $(CFLAGS) $^ -o $@ -ldb -lmpool -lfcgi -lmysqlclient_r -lcurl

clean:
	rm -f *.o queue_fcgi test
	cd mpool-2.1.0 && $(MAKE) clean

